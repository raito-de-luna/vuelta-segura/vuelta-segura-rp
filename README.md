# Vuelta Segura: Responsable Parental

Objetivo: mostrar con simpleza la DDJJ (cargada en un formulario) a través de un código QR.

## Stack tecnológico
- [jQuery](https://jquery.com/)
- [OnsenUI](https://onsen.io/)
- [Apache Cordova](https://cordova.apache.org/)

## Entorno de desarrollo general:
1. Instalar NodeJS: Bajar de [acá](https://nodejs.org/es/download/).
2. Instalar Apache Cordova: `npm -g install cordova`

### Entorno de desarrollo web:
1. Ubicarse en el directorio en el que se encuentra este repositorio: `cd directorio_en_el_que_está_el_repositorio`
2. Agregar la plataforma "Browser": `cordova platform add browser`

#### Para ejecutar:
Ejecutar el comando `cordova run browser`

### Entorno de desarrollo móvil (opcional):
1. Instalar **JDK8**: Bajar de [acá](https://www.oracle.com/ar/java/technologies/javase/javase-jdk8-downloads.html).
2. Instalar **Gradle**: Bajar de [acá](https://gradle.org/).
3. Instalar **Android Studio**: Bajar de [acá](https://developer.android.com/studio).
4. Instalar **Build Tools** 30.0.3 y 31.0.0: 
   1. Abrir Android Studio
   2. Abrir el SDK Manager: menú `Configure` > `SDK Manager`
   3. **Guardarse** la ruta que aparece en el campo `Android SDK Location` (será necesaria más tarde).
   4. Ir a la solapa `SDK Tools`
   5. Tildar la opción `[x] Show Package Details`
   6. Del listado que se despliega del paquete `Android SDK Build-Tools 31`, tildar las casillas `31.0.0` y `30.0.3`.
   7. Aplicar los cambios: click en el botón `[Apply]`
   8. Cerrar el SDK Manager: click en el botón `[OK]`
   9. Cerrar Android Studio
   10. Ir a la ruta que nos guardamos anteriormente
   11. Entrar a la carpeta `build-tools`.
   12. Entrar a la carpeta `30.0.3`.
   13. Copiarse el archivo `dx`.
   14. Volver a la carpeta anterior (`build-tools`) y entrar a la carpeta `31.0.0`.
   15. Pegar el archivo `dx` que acabamos de copiar.
5. Ubicarse en el directorio en el que se encuentra este repositorio: `cd directorio_en_el_que_está_el_repositorio`
6. Agregar la plataforma "Android": `cordova platform add android`

#### Para ejecutar:
##### Instrucciones para el dispositivo
1. Activar las "Opciones de desarrollador".
2. Activar la Depuración USB.
3. Conectar a la computadora
4. Habilitar permiso de depuración USB.

##### Instrucciones para la computadora
1. Ubicarse en el directorio en el que se encuentra este repositorio: `cd directorio_en_el_que_está_el_repositorio`
2. Ejecutar el comando `cordova run android --device`