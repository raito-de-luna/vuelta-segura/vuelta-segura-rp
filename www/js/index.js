document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!
    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    window.appNavigator = document.getElementById('appNavigator');

    window.db = window.openDatabase("vs", "1.0.0", "vs", 1024*1024*5);

    window.db.transaction(inicializarDB);

    cargarPantallPrincipal();
}

function obtenerEstudiantesParaPantallaPrincipal() {
  console.log('obtenerEstudiantesParaPantallaPrincipal');

  return new Promise(function(resolve, reject) {
    const query = `Select e.id, e.nombre, e.apellido, d.id as id_ddjj, d.fecha as fecha_ddjj
  From estudiantes e 
  Left Join ddjj d 
    On d.estudiante_id = e.id;`;
    window.db.transaction(function(tx) { 
      tx.executeSql(query, [], function(tx, resultados) {
        resolve(resultados.rows);
      });
    });
  });  
}

function declaraciónJuradaVigente(estudiante) {
  const fechaActual = new Date();
  fechaActual.setHours(0,0,0,0);

  let fechaDDJJ = new Date(estudiante.fecha_ddjj);

  const dias = (fechaActual - fechaDDJJ) / (1000*3600*24);

  return (dias >= 0 && dias < 2);
}

function formatearDatosParaPantallaPrincipal(estudiantes) {
  return new Promise(function(resolve, reject) {
    let contenido = '';

    for (let i=0; i < estudiantes.length; i++) {
      let accion = "mostrarFormularioDDJJ";
      let claseCss = "DDJJ-vencida";
      let nombre = `${estudiantes[i].apellido}, ${estudiantes[i].nombre}`;
      let id = `${estudiantes[i].id}`;
      if (declaraciónJuradaVigente(estudiantes[i])) {
        accion = "mostrarPantallaQR";
        claseCss = "DDJJ-vigente";
      }
      contenido += `
      <ons-card onclick="${accion}('${id}')" class="${claseCss}">
        <div class="title">${nombre}</div>
      </ons-card>`;
    }

    resolve(contenido);
  });
}

function cargarPantallPrincipal() {
  console.log("cargarPantallPrincipal");
  obtenerEstudiantesParaPantallaPrincipal()
  .then(function(estudiantes) {
    return formatearDatosParaPantallaPrincipal(estudiantes);
  })
  .then(function(contenido) {
    console.log("innerHTML = contenido");

    // Hack para que, no tira error por no encontrar el div "content".
    // Sí, con timeout de 0. Probamos con valores desde 1000 y fuimos bajando.
    window.setTimeout(function() {
      document.getElementById("content").innerHTML = contenido;
    }, 500);
  });
}

function inicializarDB(tx) {
  crearTablaEstudiantes(tx);
  crearTablaDDJJ(tx);
}

function crearTablaDDJJ(tx) {
  const query = `CREATE TABLE IF NOT EXISTS ddjj (
    "id" char(32) NOT NULL PRIMARY KEY,
    "fecha" date NOT NULL,
    "temperatura" real NOT NULL,
    "perdida_olfato" tinyint NOT NULL,
    "perdida_gusto" tinyint NOT NULL,
    "tos" tinyint NOT NULL,
    "dolor_garganta" tinyint NOT NULL,
    "dificultad_respiratoria" tinyint NOT NULL,
    "cefalea" tinyint NOT NULL,
    "mialgias" tinyint NOT NULL,
    "diarreas" tinyint NOT NULL,
    "vómitos" tinyint NOT NULL,
    "persona_sospechosa_covid_19" tinyint NOT NULL,
    "viaje_zona_aspo_últimos_14_dias" tinyint NOT NULL,
    "cáncer" tinyint NOT NULL,
    "diabetes" tinyint NOT NULL,
    "enf_hepática" tinyint NOT NULL,
    "enf_renal_crónica" tinyint NOT NULL,
    "enf_respiratoria" tinyint NOT NULL,
    "enf_cardiologica" tinyint NOT NULL,
    "condición_defensas_bajas" tinyint NOT NULL,
    "nombre_rp" varchar(40) NOT NULL,
    "dni_rp" varchar(9) NOT NULL,
    "estudiante_id" char(32) NOT NULL UNIQUE REFERENCES "estudiantes" ("id")
  );`;
  tx.executeSql(query, [], function(tx, resultados) {
    console.log("Tabla 'ddjj' OK")
  });

  /*
  const query2 = `INSERT INTO ddjj
(id, fecha, temperatura, perdida_olfato, perdida_gusto, tos, dolor_garganta, dificultad_respiratoria, cefalea, mialgias, diarreas, persona_sospechosa_covid_19, viaje_zona_aspo_últimos_14_dias, cáncer, diabetes, enf_hepática, enf_renal_crónica, enf_respiratoria, enf_cardiologica, condición_defensas_bajas, nombre_rp, dni_rp, estudiante_id, vómitos)
VALUES('beac6bb0-fde3-40cd-9999-a662bc18ec49', '2021-08-09', 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Aquiles Bailo', '11223344', '242d8ba3-8381-4122-9478-8107b967c19c', 0);`;
  tx.executeSql(query2, [], function(tx, resultados) {
    console.log("Registro insertado en tabla ddjj");
  });

  const query3 = `INSERT INTO ddjj
(id, fecha, temperatura, perdida_olfato, perdida_gusto, tos, dolor_garganta, dificultad_respiratoria, cefalea, mialgias, diarreas, persona_sospechosa_covid_19, viaje_zona_aspo_últimos_14_dias, cáncer, diabetes, enf_hepática, enf_renal_crónica, enf_respiratoria, enf_cardiologica, condición_defensas_bajas, nombre_rp, dni_rp, estudiante_id, vómitos)
VALUES('bd5f0226-cae6-4e23-b0bc-5068d22f009d', '2021-08-05', 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Aquiles Brinco', '11223344', '266351fe-6876-4a7d-8cd4-25ee821e0e07', 0);`;
  tx.executeSql(query3, [], function(tx, resultados) {
    console.log("Registro insertado en tabla ddjj");
  });
  */
}

function crearTablaEstudiantes(tx) {
  const query = `CREATE TABLE IF NOT EXISTS estudiantes (
    id TEXT PRIMARY KEY,
    nombre TEXT NOT NULL,
    apellido TEXT NOT NULL,
    curso TEXT NOT NULL
  );`;
  tx.executeSql(query, [], function(tx, resultados) {
    console.log("Tabla estudiantes OK");
  });

  const query2 = `INSERT INTO estudiantes (id, nombre, apellido, curso)
VALUES ("242d8ba3-8381-4122-9478-8107b967c19c", "Leonel", "Salazar", "1 B");`;
  tx.executeSql(query2, [], function(tx, resultados) {
    console.log("Registro insertado en tabla estudiantes");
  });

  const query3 = `INSERT INTO estudiantes (id, nombre, apellido, curso)
VALUES ("266351fe-6876-4a7d-8cd4-25ee821e0e07", "Benjamin", "San Juan", "1 A");`;
  tx.executeSql(query3, [], function(tx, resultados) {
    console.log("Registro insertado en tabla estudiantes");
  });
}

function confirmarDDJJ(){
  ons.notification.confirm('Declaro que la/el estudiante no tiene ninguna enfermedad cronica por la que requiera ser dispensado de la actividad presencial')
  .then(function(input) {
    if (input){
      grabarNuevaDDJJ();
      mostrarPantallaPrincipalActualizada();
    } 
  });
}

function insertarNuevaDDJJ(ddjj) {
  window.db.transaction(function(tx){
    const query = `INSERT OR REPLACE INTO ddjj (
      id,
      fecha,
      temperatura,
      perdida_olfato,
      perdida_gusto,
      tos,
      dolor_garganta,
      dificultad_respiratoria,
      cefalea,
      mialgias,
      diarreas,
      vómitos,
      persona_sospechosa_covid_19,
      viaje_zona_aspo_últimos_14_dias,
      cáncer,
      diabetes,
      enf_hepática,
      enf_renal_crónica,
      enf_respiratoria,
      enf_cardiologica,
      condición_defensas_bajas,
      nombre_rp,
      dni_rp,
      estudiante_id
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;

    tx.executeSql(query, [
      obtenerUUID(),                      /* id */ 
      obtenerFechaFormateada(new Date()), /* fecha */ 
      ddjj.temperatura,                   /* temperatura */ 
      ddjj.perdida_olfato ,               /* perdida_olfato */ 
      ddjj.perdida_gusto,                 /* perdida_gusto */ 
      ddjj.tos,                           /* tos */ 
      ddjj.dolor_garganta,                /* dolor_garganta */ 
      ddjj.dificulta_respiratoria,        /* dificultad_respiratoria */ 
      ddjj.dolor_cabeza,                  /* cefalea */ 
      ddjj.dolor_muscular,                /* mialgias */ 
      ddjj.diarrea,                       /* diarreas */ 
      ddjj.vomitos,                       /* vómitos */
      ddjj.contacto_estrecho,             /* persona_sospechosa_covid_19 */ 
      ddjj.viaje_aspo,                    /* viaje_zona_aspo_últimos_14_dias */ 
      ddjj.cancer,                        /* cáncer */ 
      ddjj.diabetes,                      /* diabetes */ 
      ddjj.enfermedad_hepatica,           /* enf_hepática */ 
      ddjj.enfermedad_cronica,            /* enf_renal_crónica */ 
      ddjj.enfermedad_respiratoria,       /* enf_respiratoria */ 
      ddjj.enfermedad_cardiologica,       /* enf_cardiologica */ 
      ddjj.defensas_bajas,                /* condición_defensas_bajas */ 
      "mingo",                            /* nombre_rp */ 
      "11223344",                         /* dni_rp */ 
      ddjj.estudiante_id                  /* estudiante_id */  
    ]);
  });
}

function grabarNuevaDDJJ() {
  const data = window.appNavigator.topPage.data;
  data["diabetes"] = document.getElementById("diabetes").checked ? 1 : 0;
  data["enfermedad_hepatica"] = document.getElementById("enfermedad_hepatica").checked ? 1 : 0;
  data["enfermedad_cronica"] = document.getElementById("enfermedad_cronica").checked ? 1 : 0;
  data["enfermedad_cardiologica"] = document.getElementById("enfermedad_cardiologica").checked ? 1 : 0;
  data["enfermedad_respiratoria"] = 0; //document.getElementById("enfermedad_respiratoria").checked ? 1 : 0;
  data["cancer"] = 0; //document.getElementById("cancer").checked ? 1 : 0;
  data["defensas_bajas"] = document.getElementById("defensas_bajas").checked ? 1 : 0;
  data["contacto_estrecho"] = document.getElementById("contacto_estrecho").checked ? 1 : 0;
  data["viaje_aspo"] = document.getElementById("viaje_aspo").checked ? 1 : 0;

  insertarNuevaDDJJ(data);
}

function obtenerFechaFormateada(fecha) {
  const año = fecha.getFullYear();
  const mes = (fecha.getMonth() + 1).toString().padStart(2, "0");
  const dia = fecha.getDate().toString().padStart(2, "0");

  return `${año}-${mes}-${dia}`;
}

function obtenerUUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (dt + Math.random()*16)%16 | 0;
      dt = Math.floor(dt/16);
      return (c=='x' ? r :(r&0x3|0x8)).toString(16);
  });
  return uuid;
}

function mostrarPantallaPrincipalActualizada(){
  window.appNavigator.resetToPage('html/main.html');
  cargarPantallPrincipal();
}

function mostrarPantallaQR(estudiante_id) {
  document.getElementById('appNavigator')
  .pushPage('html/CodigoQR.html')
  .then(function() {
    return obtenerInformacionParaPantallaQR(estudiante_id)
  })
  .then(function (datosPantallaQR) {
    cargarInformaciónEnPantallaQR(datosPantallaQR);
  });
}

function mostrarFormularioDDJJ(estudiante_id) {
  document.getElementById('appNavigator')
  .pushPage('html/formulario-DDJJ.html', { 'data': {estudiante_id: estudiante_id} });
}

function mostrarFormulario2DDJJ() {
  const data = {};
  data["estudiante_id"] = window.appNavigator.topPage.data.estudiante_id;
  data["temperatura"] = document.getElementById("temperatura").value;
  data["perdida_olfato"] = document.getElementById("olfato_si").checked ? 1 : 0;
  data["perdida_gusto"] = document.getElementById("gusto_si").checked ? 1 : 0;
  data["tos"] = document.getElementById("tos_si").checked ? 1 : 0;
  data["dolor_garganta"] = document.getElementById("garganta_si").checked ? 1 : 0;
  data["dificulta_respiratoria"] = document.getElementById("respirar_si").checked ? 1 : 0;
  data["dolor_cabeza"] = document.getElementById("cabeza_si").checked ? 1 : 0;
  data["dolor_muscular"] = document.getElementById("muscular_si").checked ? 1 : 0;
  data["diarrea"] = document.getElementById("diarrea_si").checked ? 1 : 0;
  data["vomitos"] = document.getElementById("vomitos_si").checked ? 1 : 0;

  window.appNavigator.pushPage(
    'html/formulario-DDJJ-2.html', { 'data': data }
  );
}

function mostrarDDJJ(estudiante_id){
  document.getElementById('appNavigator')
  .pushPage('html/ddjj.html', { 'data': estudiante_id })
  .then(function(){
    console.log("obtener datos de DDJJ");
    console.log("Cargar datos en pantalla");
  });
}

function mostrarNombreEstudiante(estudiante) {
  document.getElementById("nombreEstudiante").innerText = `${estudiante["nombre"]} ${estudiante["apellido"]}`;
}

function cargarInformaciónEnPantallaQR(datos) {
  mostrarNombreEstudiante(datos["estudiante"]);
  generarQR(JSON.stringify(datos));
}

function obtenerInformacionParaPantallaQR(estudiante_id){
  let resultado = {};

  return new Promise(function(resolve, reject) {
    obtenerEstudiantePorID(estudiante_id)
    .then(function (estudiante) {
      resultado["estudiante"] = estudiante;
      return obtenerDDJJPorEstudiante(estudiante_id);
    })
    .then(function(ddjj) {
      resultado["ddjj"] = ddjj;
      resolve(resultado);
    });
  });
}

function obtenerEstudiantePorID(estudiante_id){
  return new Promise(function(resolve, reject) {
    window.db.transaction(function(tx) {
      const query = "Select * From estudiantes Where id = ?;";
      db.transaction(function(tx) {
        tx.executeSql(query, [estudiante_id], function(tx, resultados) {
          resolve(resultados.rows[0]);
        });
      });
    });
  });
}

function obtenerDDJJPorEstudiante(estudiante_id) {
  return new Promise(function(resolve, reject) {
    window.db.transaction(function(tx) {
      const query = "Select * From ddjj Where estudiante_id = ?;";
      db.transaction(function(tx) {
        tx.executeSql(query, [estudiante_id], function(tx, resultados) {
          resolve(resultados.rows[0]);
        });
      });
    });
  });
}

function generarQR(ddjjFormatoJson) {
  var qr = new QRious({
    element: document.getElementById('qr'),
    size: 700,
    value: ddjjFormatoJson
  });
}

window.fn = {};

window.fn.toggleMenu = function () {
  document.getElementById('appSplitter').right.toggle();
};

window.fn.loadView = function (index) {
  document.getElementById('appTabbar').setActiveTab(index);
  document.getElementById('sidemenu').close();
};

window.fn.loadLink = function (url) {
  window.open(url, '_blank');
};

window.fn.pushPage = function (page, anim) {
  if (anim) {
    document.getElementById('appNavigator').pushPage(page.id, { data: { title: page.title }, animation: anim });
  } else {
    document.getElementById('appNavigator').pushPage(page.id, { data: { title: page.title } });
  }
};

function verDatosRP(){
  appNavigator.pushPage('html/datos-rp.html');
}

function verListadoDeHijos(){
  appNavigator.pushPage('html/listado-hijos.html');
}

function verDDJJCargadas(){
  appNavigator.pushPage('html/ddjj-cargadas.html');
}

